/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include <debug/sahtrace.h>

#include <pcb/utils.h>
#include <pcb/core.h>

#include "nemo/client.h"
#include "client/util.h"

variant_t* nemo_getDHCPOption(const char* intf, const char* type, uint16_t tag, const char* traverse) {
    request_t* req = request_create_executeFunction(nemo_intf2path(intf), "getDHCPOption", request_common_path_key_notation | request_function_args_by_name);
    if(type) {
        request_addCharParameter(req, "type", type);
    }
    if(tag) {
        request_addUInt16Parameter(req, "tag", tag);
    }
    if(traverse) {
        request_addCharParameter(req, "traverse", traverse);
    }
    nemo_request_invoke(req);
    variant_t* retval = calloc(1, sizeof(variant_t));
    variant_initialize(retval, variant_type_unknown);
    variant_copy(retval, nemo_request_returnValue(req));
    request_destroy(req);
    return retval;
}

nemo_query_t* nemo_openQuery_getDHCPOption(const char* intf, const char* subscriber, const char* type, uint16_t tag, const char* traverse, nemo_result_handler_t handler, void* userdata) {
    argument_value_list_t args;
    argument_value_list_initialize(&args);
    if(type) {
        nemo_argument_valueAddChar(&args, "type", type);
    }
    nemo_argument_valueAddUInt16(&args, "tag", tag);
    if(traverse) {
        nemo_argument_valueAddChar(&args, "traverse", traverse);
    }
    nemo_query_t* query = nemo_openQuery(intf, subscriber, "getDHCPOption", &args, request_function_args_by_name, handler, userdata);
    argument_valueClear(&args);
    return query;
}
