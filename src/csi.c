/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <signal.h>
#include <string.h>

#include <debug/sahtrace.h>

#include <pcb/utils.h>
#include <pcb/core.h>

#include "nemo/client.h"
#include "client/util.h"

#define NOTIFY_NEMO_CSI_EXEC (notify_custom + 2)
#define NOTIFY_NEMO_CSI_CANCEL (notify_custom + 3)

typedef struct _csi {
    llist_iterator_t it;
    char* intf;
    char* func;
    nemo_exec_handler_t exec;
    nemo_cancel_handler_t cancel;
    void* userdata;
    request_t* request;
    llist_t calls;
} csi_t;

typedef struct _nemo_csicall {
    llist_iterator_t it;
    csi_t* csi;
    uint32_t id;
    void* userdata;
    argument_value_list_t args;
    uint32_t attributes;
    variant_t retval;
    llist_t errors;
} csicall_t;

typedef struct csierror {
    llist_iterator_t it;
    uint32_t code;
    char* description;
    char* info;
} csierror_t;

static llist_t csis = {NULL, NULL};

static void csierror_destroy(csierror_t* csierror) {
    if(!csierror) {
        return;
    }
    llist_iterator_take(&csierror->it);
    free(csierror->description);
    free(csierror->info);
    free(csierror);
}

static csierror_t* csierror_create(csicall_t* csicall, uint32_t code, const char* description, const char* info) {
    csierror_t* csierror = calloc(1, sizeof(csierror_t));
    if(!csierror) {
        SAH_TRACE_ERROR("nemo - calloc failed");
        goto error;
    }
    llist_append(&csicall->errors, &csierror->it);
    csierror->code = code;
    csierror->description = description ? strdup(description) : NULL;
    if(description && !csierror->description) {
        SAH_TRACE_ERROR("nemo - strdup(%s) failed", description);
        goto error;
    }
    csierror->info = info ? strdup(info) : NULL;
    if(info && !csierror->info) {
        SAH_TRACE_ERROR("nemo - strdup(%s) failed", info);
        goto error;
    }
    return csierror;
error:
    csierror_destroy(csierror);
    return NULL;
}

static csicall_t* csicall_create(csi_t* csi, uint32_t id) {
    csicall_t* csicall = calloc(1, sizeof(csicall_t));
    if(!csicall) {
        SAH_TRACE_ERROR("nemo - calloc failed");
        return NULL;
    }
    llist_append(&csi->calls, &csicall->it);
    csicall->csi = csi;
    csicall->id = id;
    csicall->userdata = csi->userdata;
    argument_value_list_initialize(&csicall->args);
    variant_initialize(&csicall->retval, variant_type_unknown);
    return csicall;
}

static csicall_t* csicall_find(csi_t* csi, uint32_t id) {
    csicall_t* csicall;
    for(csicall = (csicall_t*) llist_first(&csi->calls); csicall; csicall = (csicall_t*) llist_iterator_next(&csicall->it)) {
        if(csicall->id == id) {
            return csicall;
        }
    }
    return NULL;
}

static void csicall_destroy(csicall_t* csicall) {
    if(!csicall) {
        return;
    }
    llist_iterator_take(&csicall->it);
    argument_valueClear(&csicall->args);
    variant_cleanup(&csicall->retval);
    while(!llist_isEmpty(&csicall->errors)) {
        csierror_destroy((csierror_t*) llist_first(&csicall->errors));
    }
    free(csicall);
}

static csi_t* csi_find(const char* intf, const char* func) {
    csi_t* csi;
    for(csi = (csi_t*) llist_first(&csis); csi; csi = (csi_t*) llist_iterator_next(&csi->it)) {
        if(!strcmp(csi->intf, intf) && !strcmp(csi->func, func)) {
            return csi;
        }
    }
    return NULL;
}

static void csi_destroy(csi_t* csi) {
    if(!csi) {
        return;
    }
    llist_iterator_take(&csi->it);
    while(!llist_isEmpty(&csi->calls)) {
        csicall_destroy((csicall_t*) llist_first(&csi->calls));
    }
    if(csi->request) {
        request_destroy(csi->request);
    }
    free(csi->intf);
    free(csi->func);
    free(csi);
}

static bool csi_replyHandler(request_t* req, reply_item_t* item, pcb_t* pcb, peer_info_t* from, void* userdata) {
    (void) req; (void) pcb; (void) from;
    csi_t* csi = (csi_t*) userdata;
    if(reply_item_type(item) != reply_type_notification) {
        return true;
    }
    notification_t* notification = reply_item_notification(item);
    if((notification_type(notification) != NOTIFY_NEMO_CSI_EXEC) && (notification_type(notification) != NOTIFY_NEMO_CSI_CANCEL)) {
        return true;
    }
    notification_parameter_t* parameter;

    parameter = notification_getParameter(notification, "intf");
    char* intf = variant_char(notification_parameter_variant(parameter));
    notification_parameter_destroy(parameter);

    parameter = notification_getParameter(notification, "func");
    char* func = variant_char(notification_parameter_variant(parameter));
    notification_parameter_destroy(parameter);

    parameter = notification_getParameter(notification, "id");
    uint32_t id = variant_uint32(notification_parameter_variant(parameter));
    notification_parameter_destroy(parameter);

    if(!intf || !func) {
        SAH_TRACE_ERROR("nemo - Unexpected error: nemo csi notification without intf or func parameter");
        return true;
    }

    if(strcmp(intf, csi->intf) || strcmp(func, csi->func)) {
        goto leave; // notification not belonging to this csi received - ignore silently.

    }
    if(notification_type(notification) == NOTIFY_NEMO_CSI_EXEC) {
        csicall_t* csicall = csicall_create(csi, id);
        if(!csicall) {
            goto leave;
        }

        parameter = notification_getParameter(notification, "attributes");
        csicall->attributes = variant_uint32(notification_parameter_variant(parameter));
        notification_parameter_destroy(parameter);

        for(parameter = notification_firstParameter(notification); parameter; parameter = notification_nextParameter(parameter)) {
            argument_valueAdd(&csicall->args, notification_parameter_name(parameter), notification_parameter_variant(parameter));
        }

        function_exec_state_t state = function_exec_error;

        if(csi->exec) {
            state = csi->exec(csicall);
        }

        if(state != function_exec_executing) {
            nemo_csicall_finish(csicall, state);
        }
    } else if(notification_type(notification) == NOTIFY_NEMO_CSI_CANCEL) {
        csicall_t* csicall = csicall_find(csi, id);
        if(!csicall) {
            goto leave;
        }

        if(csi->cancel) {
            csi->cancel(csicall);
        }

        csicall_destroy(csicall);
    }
leave:
    free(intf);
    free(func);
    return true;
}

static csi_t* csi_create(const char* intf, const char* func, nemo_exec_handler_t exec, nemo_cancel_handler_t cancel, void* userdata) {
    csi_t* csi = calloc(1, sizeof(csi_t));
    if(!csi) {
        SAH_TRACE_ERROR("nemo - calloc failed");
        goto error;
    }
    llist_append(&csis, &csi->it);
    csi->intf = strdup(intf);
    if(!csi->intf) {
        SAH_TRACE_ERROR("nemo - strdup(%s) failed", intf);
        goto error;
    }
    csi->func = strdup(func);
    if(!csi->func) {
        SAH_TRACE_ERROR("nemo - strdup(%s) failed", func);
        goto error;
    }
    csi->exec = exec;
    csi->cancel = cancel;
    csi->userdata = userdata;

    csi->request = request_create_getObject(nemo_intf2path(intf), 0, request_no_object_caching | request_common_path_key_notation
                                            | request_notify_custom | request_notify_only | request_notify_no_updates);
    request_setData(csi->request, csi);
    request_setReplyItemHandler(csi->request, csi_replyHandler);
    SAH_TRACEZ_INFO("nemo", "send request %s?&", request_path(csi->request));
    if(!pcb_sendRequest(nemo_client_pcb(), nemo_client_peer(), csi->request)) {
        SAH_TRACE_ERROR("nemo - Failed to send request: %d (%s)", pcb_error, error_string(pcb_error));
        goto error;
    }

    return csi;
error:
    csi_destroy(csi);
    return NULL;
}

void nemo_csiRegister(const char* intf, const char* func, nemo_exec_handler_t exec, nemo_cancel_handler_t cancel, void* userdata) {
    if(!intf) {
        SAH_TRACE_ERROR("nemo - intf is NULL");
        return;
    }
    if(!func) {
        SAH_TRACE_ERROR("nemo - func is NULL");
        return;
    }
    csi_t* csi = csi_create(intf, func, exec, cancel, userdata);
    if(!csi) {
        return;
    }
    request_t* req = request_create_executeFunction(nemo_intf2path(intf), "csiRegister", request_common_path_key_notation | request_function_args_by_name);
    request_addCharParameter(req, "func", func);
    nemo_request_invoke(req);
    request_destroy(req);
}

void nemo_csiUnregister(const char* intf, const char* func) {
    if(!intf) {
        SAH_TRACE_ERROR("nemo - intf is NULL");
        return;
    }
    if(!func) {
        SAH_TRACE_ERROR("nemo - func is NULL");
        return;
    }
    csi_t* csi = csi_find(intf, func);
    if(!csi) {
        SAH_TRACE_ERROR("nemo - csi[intf==%s func==%s] not found", intf, func);
        return;
    }
    request_t* req = request_create_executeFunction(nemo_intf2path(intf), "csiUnregister", request_common_path_key_notation | request_function_args_by_name);
    request_addCharParameter(req, "func", func);
    nemo_request_invoke(req);
    csi_destroy(csi);
    request_destroy(req);
}

const char* nemo_csicall_intf(nemo_csicall_t* csicall) {
    if(!csicall) {
        SAH_TRACE_ERROR("nemo - csicall is NULL");
        return NULL;
    }
    return csicall->csi->intf;
}

const char* nemo_csicall_func(nemo_csicall_t* csicall) {
    if(!csicall) {
        SAH_TRACE_ERROR("nemo - csicall is NULL");
        return NULL;
    }
    return csicall->csi->func;
}

void* nemo_csicall_userdata(nemo_csicall_t* csicall) {
    if(!csicall) {
        SAH_TRACE_ERROR("nemo - csicall is NULL");
        return NULL;
    }
    return csicall->userdata;
}

void nemo_csicall_setUserdata(nemo_csicall_t* csicall, void* userdata) {
    if(!csicall) {
        SAH_TRACE_ERROR("nemo - csicall is NULL");
        return;
    }
    csicall->userdata = userdata;
}

argument_value_list_t* nemo_csicall_arguments(nemo_csicall_t* csicall) {
    if(!csicall) {
        SAH_TRACE_ERROR("nemo - csicall is NULL");
        return NULL;
    }
    return &csicall->args;
}

uint32_t nemo_csicall_attributes(nemo_csicall_t* csicall) {
    if(!csicall) {
        SAH_TRACE_ERROR("nemo - csicall is NULL");
        return 0;
    }
    return csicall->attributes;
}

variant_t* nemo_csicall_returnValue(nemo_csicall_t* csicall) {
    if(!csicall) {
        SAH_TRACE_ERROR("nemo - csicall is NULL");
        return NULL;
    }
    return &csicall->retval;
}

void nemo_csicall_addError(nemo_csicall_t* csicall, uint32_t code, const char* description, const char* info) {
    if(!csicall) {
        SAH_TRACE_ERROR("nemo - csicall is NULL");
        return;
    }
    csierror_create(csicall, code, description, info);
}

void nemo_csicall_finish(nemo_csicall_t* csicall, function_exec_state_t state) {
    if(!csicall) {
        SAH_TRACE_ERROR("nemo - csicall is NULL");
        return;
    }
    request_t* req = request_create_executeFunction(nemo_intf2path(csicall->csi->intf), "csiFinish", request_common_path_key_notation | request_function_args_by_name);
    request_addUInt32Parameter(req, "id", csicall->id);
    request_addCharParameter(req, "state", state == function_exec_done ? "done" : "error");

    if(variant_type(&csicall->retval) != variant_type_unknown) {
        request_addParameter(req, "returnValue", &csicall->retval);
    }

    if(!llist_isEmpty(&csicall->errors)) {
        variant_list_t errors;
        variant_list_initialize(&errors);
        csierror_t* csierror;
        for(csierror = (csierror_t*) llist_first(&csicall->errors); csierror; csierror = (csierror_t*) llist_iterator_next(&csierror->it)) {
            variant_map_t map;
            variant_map_initialize(&map);
            variant_map_addUInt32(&map, "code", csierror->code);
            if(csierror->description) {
                variant_map_addChar(&map, "description", csierror->description);
            }
            if(csierror->info) {
                variant_map_addChar(&map, "info", csierror->info);
            }
            variant_list_addMap(&errors, &map);
            variant_map_cleanup(&map);
        }
        request_addListParameter(req, "errors", &errors);
        variant_list_cleanup(&errors);
    }

    argument_value_t* arg;
    for(arg = argument_valueFirstArgument(&csicall->args); arg; arg = argument_valueNextArgument(arg)) {
        request_addParameter(req, argument_valueName(arg), argument_value(arg));
    }

    nemo_request_invoke(req);

    request_destroy(req);

    csicall_destroy(csicall);
}

