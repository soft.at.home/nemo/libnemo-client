/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <signal.h>
#include <string.h>

#include <debug/sahtrace.h>

#include <pcb/utils.h>
#include <pcb/core.h>

#include "nemo/client.h"
#include "client/util.h"

static pcb_t* curpcb = NULL;
static peer_info_t* curpeer = NULL;
static bool ownpeer = false;
static int refcount = 0;

static bool nemo_client_closeHandler(peer_info_t* peer) {
    if(peer != curpeer) {
        return true;
    }
    curpeer = NULL;
    ownpeer = false;
    return true;
}

static bool nemo_client_connect(const char* uristr) {
    if(!uristr) {
        return false;
    }
    uri_t* uri = NULL;
    SAH_TRACEZ_INFO("nemo", "Connecting to %s ...", uristr);
    curpeer = connection_connect(pcb_connection(curpcb), uristr, &uri);
    if(curpeer) {
        SAH_TRACEZ_INFO("nemo", "Connected to NeMo via %s", uristr);
        ownpeer = true;
        peer_addCloseHandler(curpeer, nemo_client_closeHandler);
    } else {
        SAH_TRACEZ_INFO("nemo", "Connection to %s failed", uristr);
    }
    uri_destroy(uri);
    return curpeer ? true : false;
}

bool nemo_client_initialize(pcb_t* pcb, peer_info_t* peer) {
    if(refcount++) {
        return curpeer ? true : false;
    }
    curpcb = pcb;
    curpeer = NULL;
    ownpeer = false;
    if(nemo_client_connect(getenv("NEMO_URI"))) {
        return true;
    }
    if(nemo_client_connect("pcb://ipc:[/var/run/nemo]")) {
        return true;
    }
    if(nemo_client_connect(getenv("PCB_SYS_BUS"))) {
        return true;
    }
    if(nemo_client_connect("pcb://ipc:[/var/run/pcb_sys]")) {
        return true;
    }
    if(peer) {
        curpeer = peer;
        SAH_TRACEZ_INFO("nemo", "Connected to NeMo using caller-provided peer.");
        return true;
    }
    SAH_TRACE_ERROR("nemo - Could not connect to NeMo after having tried all possible methods!");
    return false;
}

void nemo_client_cleanup() {
    if(--refcount) {
        return;
    }
    if(ownpeer) {
        peer_destroy(curpeer);
    }
    curpeer = NULL;
    curpcb = NULL;
    ownpeer = false;
}

pcb_t* nemo_client_pcb() {
    return curpcb;
}

peer_info_t* nemo_client_peer() {
    return curpeer;
}

const char* nemo_intf2path(const char* intf) {
    static char pathbuf[64];
    snprintf(pathbuf, 64, "%s.Intf.%s", ROOT_OBJ_NAME, intf ? : "lo");
    return pathbuf;
}

bool nemo_request_invoke(request_t* req) {
    if(sahTraceZoneLevel(sahTraceGetZone("nemo")) >= TRACE_LEVEL_INFO) {
        string_t s;
        int n = 0;
        string_initialize(&s, 64);
        parameter_iterator_t* it;
        for(it = request_firstParameter(req); it; it = request_nextParameter(it)) {
            char* charvalue = request_parameterValue(it) ? variant_char(request_parameterValue(it)) : NULL;
            string_appendFormat(&s, "%s%s%s%s", n++ ? ", " : "", request_parameterName(it), charvalue ? "=" : "", charvalue ? charvalue : "");
            free(charvalue);
        }
        SAH_TRACEZ_INFO("nemo", "send request %s.%s(%s)", request_path(req), request_functionName(req), string_buffer(&s) ? string_buffer(&s) : "");
        string_cleanup(&s);
    }
    if(!pcb_sendRequest(curpcb, curpeer, req)) {
        SAH_TRACE_ERROR("nemo - Failed to send request: %d (%s)", pcb_error, error_string(pcb_error));
        return false;
    }

    struct timeval timeout;
    timeout.tv_sec = 30;
    timeout.tv_usec = 0;

    if(!pcb_waitForReply(curpcb, req, &timeout)) {
        SAH_TRACE_ERROR("nemo - Communication failed");
        return false;
    }

    reply_t* reply = request_reply(req);
    if(!reply) {
        SAH_TRACE_ERROR("nemo - No reply available");
        return false;
    }

    reply_item_t* item;
    for(item = reply_firstItem(reply); item; item = reply_nextItem(item)) {
        if(reply_item_type(item) == reply_type_error) {
            SAH_TRACE_ERROR("nemo - Function call error (%d) - %s - %s",
                            reply_item_error(item), reply_item_errorDescription(item), reply_item_errorInfo(item));
            return false;
        }
    }
    return true;
}

const variant_t* nemo_request_returnValue(request_t* req) {
    reply_item_t* item;
    for(item = reply_firstItem(request_reply(req)); item; item = reply_nextItem(item)) {
        if(reply_item_type(item) == reply_type_function_return) {
            return reply_item_returnValue(item);
        }
    }
    return NULL;
}

void nemo_argument_valueAddChar(argument_value_list_t* args, const char* name, const char* value) {
    variant_t var;
    variant_initialize(&var, variant_type_string);
    variant_setChar(&var, value);
    argument_valueAdd(args, name, &var);
    variant_cleanup(&var);
}

void nemo_argument_valueAddUInt16(argument_value_list_t* args, const char* name, uint16_t value) {
    variant_t var;
    variant_initialize(&var, variant_type_uint16);
    variant_setUInt16(&var, value);
    argument_valueAdd(args, name, &var);
    variant_cleanup(&var);
}

