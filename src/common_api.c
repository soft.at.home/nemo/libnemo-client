/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <signal.h>
#include <string.h>

#include <debug/sahtrace.h>

#include <pcb/utils.h>
#include <pcb/core.h>

#include "nemo/client.h"
#include "client/util.h"

const char* nemo_traverse_this = "this";
const char* nemo_traverse_down = "down";
const char* nemo_traverse_up = "up";
const char* nemo_traverse_down_exclusive = "down exclusive";
const char* nemo_traverse_up_exclusive = "up exclusive";
const char* nemo_traverse_one_level_down = "one level down";
const char* nemo_traverse_one_level_up = "one level up";
const char* nemo_traverse_all = "all";

static void nemo_query_destroy(nemo_query_t* query);

bool nemo_isUp(const char* intf, const char* flag, const char* traverse) {
    request_t* req = request_create_executeFunction(nemo_intf2path(intf), "isUp", request_common_path_key_notation | request_function_args_by_name);
    if(flag) {
        request_addCharParameter(req, "flag", flag);
    }
    if(traverse) {
        request_addCharParameter(req, "traverse", traverse);
    }
    nemo_request_invoke(req);
    bool retval = variant_bool(nemo_request_returnValue(req));
    request_destroy(req);
    return retval;
}

bool nemo_hasFlag(const char* intf, const char* flag, const char* condition, const char* traverse) {
    request_t* req = request_create_executeFunction(nemo_intf2path(intf), "hasFlag", request_common_path_key_notation | request_function_args_by_name);
    if(flag) {
        request_addCharParameter(req, "flag", flag);
    }
    if(condition) {
        request_addCharParameter(req, "condition", condition);
    }
    if(traverse) {
        request_addCharParameter(req, "traverse", traverse);
    }
    nemo_request_invoke(req);
    bool retval = variant_bool(nemo_request_returnValue(req));
    request_destroy(req);
    return retval;
}

void nemo_setFlag(const char* intf, const char* flag, const char* condition, const char* traverse) {
    request_t* req = request_create_executeFunction(nemo_intf2path(intf), "setFlag", request_common_path_key_notation | request_function_args_by_name);
    if(flag) {
        request_addCharParameter(req, "flag", flag);
    }
    if(condition) {
        request_addCharParameter(req, "condition", condition);
    }
    if(traverse) {
        request_addCharParameter(req, "traverse", traverse);
    }
    nemo_request_invoke(req);
    request_destroy(req);
}

void nemo_clearFlag(const char* intf, const char* flag, const char* condition, const char* traverse) {
    request_t* req = request_create_executeFunction(nemo_intf2path(intf), "clearFlag", request_common_path_key_notation | request_function_args_by_name);
    if(flag) {
        request_addCharParameter(req, "flag", flag);
    }
    if(condition) {
        request_addCharParameter(req, "condition", condition);
    }
    if(traverse) {
        request_addCharParameter(req, "traverse", traverse);
    }
    nemo_request_invoke(req);
    request_destroy(req);
}

bool nemo_isLinkedTo(const char* intf, const char* target, const char* traverse) {
    request_t* req = request_create_executeFunction(nemo_intf2path(intf), "isLinkedTo", request_common_path_key_notation | request_function_args_by_name);
    if(target) {
        request_addCharParameter(req, "target", target);
    }
    if(traverse) {
        request_addCharParameter(req, "traverse", traverse);
    }
    nemo_request_invoke(req);
    bool retval = variant_bool(nemo_request_returnValue(req));
    request_destroy(req);
    return retval;
}

variant_list_t* nemo_getIntfs(const char* intf, const char* flag, const char* traverse) {
    request_t* req = request_create_executeFunction(nemo_intf2path(intf), "getIntfs", request_common_path_key_notation | request_function_args_by_name);
    if(flag) {
        request_addCharParameter(req, "flag", flag);
    }
    if(traverse) {
        request_addCharParameter(req, "traverse", traverse);
    }
    nemo_request_invoke(req);
    variant_list_t* retval = variant_array(nemo_request_returnValue(req));
    request_destroy(req);
    return retval;
}

char* nemo_luckyIntf(const char* intf, const char* flag, const char* traverse) {
    request_t* req = request_create_executeFunction(nemo_intf2path(intf), "luckyIntf", request_common_path_key_notation | request_function_args_by_name);
    if(flag) {
        request_addCharParameter(req, "flag", flag);
    }
    if(traverse) {
        request_addCharParameter(req, "traverse", traverse);
    }
    nemo_request_invoke(req);
    char* retval = variant_char(nemo_request_returnValue(req));
    request_destroy(req);
    return retval;
}

variant_t* nemo_getFirstParameter(const char* intf, const char* name, const char* flag, const char* traverse) {
    request_t* req = request_create_executeFunction(nemo_intf2path(intf), "getFirstParameter", request_common_path_key_notation | request_function_args_by_name);
    if(name) {
        request_addCharParameter(req, "name", name);
    }
    if(flag) {
        request_addCharParameter(req, "flag", flag);
    }
    if(traverse) {
        request_addCharParameter(req, "traverse", traverse);
    }
    nemo_request_invoke(req);
    variant_t* retval = calloc(1, sizeof(variant_t));
    variant_initialize(retval, variant_type_unknown);
    variant_copy(retval, nemo_request_returnValue(req));
    request_destroy(req);
    return retval;
}

variant_map_t* nemo_getParameters(const char* intf, const char* name, const char* flag, const char* traverse) {
    request_t* req = request_create_executeFunction(nemo_intf2path(intf), "getParameters", request_common_path_key_notation | request_function_args_by_name);
    if(name) {
        request_addCharParameter(req, "name", name);
    }
    if(flag) {
        request_addCharParameter(req, "flag", flag);
    }
    if(traverse) {
        request_addCharParameter(req, "traverse", traverse);
    }
    nemo_request_invoke(req);
    variant_map_t* retval = variant_map(nemo_request_returnValue(req));
    request_destroy(req);
    return retval;
}

void nemo_setFirstParameter(const char* intf, const char* name, const variant_t* value, const char* flag, const char* traverse) {
    request_t* req = request_create_executeFunction(nemo_intf2path(intf), "setFirstParameter", request_common_path_key_notation | request_function_args_by_name);
    if(name) {
        request_addCharParameter(req, "name", name);
    }
    if(value) {
        request_addParameter(req, "value", value);
    }
    if(flag) {
        request_addCharParameter(req, "flag", flag);
    }
    if(traverse) {
        request_addCharParameter(req, "traverse", traverse);
    }
    nemo_request_invoke(req);
    request_destroy(req);
}

void nemo_setParameters(const char* intf, const char* name, const variant_t* value, const char* flag, const char* traverse) {
    request_t* req = request_create_executeFunction(nemo_intf2path(intf), "setParameters", request_common_path_key_notation | request_function_args_by_name);
    if(name) {
        request_addCharParameter(req, "name", name);
    }
    if(value) {
        request_addParameter(req, "value", value);
    }
    if(flag) {
        request_addCharParameter(req, "flag", flag);
    }
    if(traverse) {
        request_addCharParameter(req, "traverse", traverse);
    }
    nemo_request_invoke(req);
    request_destroy(req);
}

variant_map_t* nemo_getMIBs(const char* intf, const char* mibs, const char* flag, const char* traverse) {
    request_t* req = request_create_executeFunction(nemo_intf2path(intf), "getMIBs", request_common_path_key_notation | request_function_args_by_name);
    if(mibs) {
        request_addCharParameter(req, "mibs", mibs);
    }
    if(flag) {
        request_addCharParameter(req, "flag", flag);
    }
    if(traverse) {
        request_addCharParameter(req, "traverse", traverse);
    }
    nemo_request_invoke(req);
    variant_map_t* retval = variant_map(nemo_request_returnValue(req));
    request_destroy(req);
    return retval;
}

void nemo_setMIBs(const char* intf, variant_map_t* mibs) {
    request_t* req = request_create_executeFunction(nemo_intf2path(intf), "setMIBs", request_common_path_key_notation | request_function_args_by_name);
    if(mibs) {
        request_addMapParameter(req, "mibs", mibs);
    }
    nemo_request_invoke(req);
    request_destroy(req);
}

void nemo_linkIntfs(const char* ulintf, const char* llintf) {
    request_t* req = request_create_executeFunction(ROOT_OBJ_NAME, "linkIntfs", request_common_path_key_notation | request_function_args_by_name);
    if(ulintf) {
        request_addCharParameter(req, "ulintf", ulintf);
    }
    if(llintf) {
        request_addCharParameter(req, "llintf", llintf);
    }
    nemo_request_invoke(req);
    request_destroy(req);
}

void nemo_unlinkIntfs(const char* ulintf, const char* llintf) {
    request_t* req = request_create_executeFunction(ROOT_OBJ_NAME, "unlinkIntfs", request_common_path_key_notation | request_function_args_by_name);
    if(ulintf) {
        request_addCharParameter(req, "ulintf", ulintf);
    }
    if(llintf) {
        request_addCharParameter(req, "llintf", llintf);
    }
    nemo_request_invoke(req);
    request_destroy(req);
}

typedef struct _query_request {
    llist_iterator_t it;
    request_t* request;
    uint32_t query_id;
    int refcount;
} query_request_t;

struct _nemo_query {
    llist_iterator_t it;
    char* intf;
    char* subscriber;
    char* cl;
    argument_value_list_t args;
    uint32_t attributes;
    nemo_result_handler_t handler;
    void* userdata;
    query_request_t* request;
    bool to_destroy;
};

static llist_t queries = {NULL, NULL};
static bool handling = false;
static llist_t requests = {NULL, NULL};

static bool query_replyHandler(request_t* req, reply_item_t* item, pcb_t* pcb, peer_info_t* from, void* userdata) {
    (void) req; (void) pcb; (void) from;
    query_request_t* request = (query_request_t*) userdata;
    if(reply_item_type(item) != reply_type_notification) {
        return true;
    }
    notification_t* notification = reply_item_notification(item);
    variant_t* result = notification_parameter_variant(notification_getParameter(notification, "Result"));
    if(!result) {
        return true;
    }
    nemo_query_t* query = NULL;
    nemo_query_t* query_next = NULL;
    handling = true;
    for(query = (nemo_query_t*) llist_first(&queries); query; query = query_next) {
        query_next = (nemo_query_t*) llist_iterator_next(&query->it);
        if((query->request == request) && query->handler) {
            query->handler(result, query->userdata);
        }
    }
    handling = false;
    for(query = (nemo_query_t*) llist_first(&queries); query; query = query_next) {
        query_next = (nemo_query_t*) llist_iterator_next(&query->it);
        if(query->to_destroy) {
            llist_iterator_take(&query->it);
            nemo_query_destroy(query);
        }
    }

    return true;
}

static void query_request_destroy(query_request_t* request) {
    if(!request) {
        return;
    }
    if(request->request) {
        SAH_TRACEZ_INFO("nemo", "cancel request %s?&", request_path(request->request));
        request_destroy(request->request);
    }
    llist_iterator_take(&request->it);
    free(request);
}

static query_request_t* query_request_create(const char* intf, uint32_t query_id) {
    query_request_t* request = calloc(1, sizeof(query_request_t));
    if(!request) {
        SAH_TRACE_ERROR("nemo - calloc failed");
        goto error;
    }
    request->query_id = query_id;
    static char querypath[64];
    snprintf(querypath, 64, "%s.Intf.%s.Query.%u", ROOT_OBJ_NAME, intf, query_id);
    request->request = request_create_getObject(querypath, 0, request_no_object_caching | request_common_path_key_notation | request_notify_custom | request_notify_only);
    request_setData(request->request, request);
    request_setReplyItemHandler(request->request, query_replyHandler);
    SAH_TRACEZ_INFO("nemo", "send request %s?&", request_path(request->request));
    if(!pcb_sendRequest(nemo_client_pcb(), nemo_client_peer(), request->request)) {
        SAH_TRACE_ERROR("nemo - Failed to send request: %d (%s)", pcb_error, error_string(pcb_error));
        goto error;
    }
    llist_append(&requests, &request->it);
    return request;
error:
    query_request_destroy(request);
    return NULL;
}

static query_request_t* query_request_find(uint32_t query_id) {
    query_request_t* request;
    for(request = (query_request_t*) llist_first(&requests); request; request = (query_request_t*) llist_iterator_next(&request->it)) {
        if(request->query_id == query_id) {
            return request;
        }
    }
    return NULL;
}

static void nemo_query_destroy(nemo_query_t* query) {
    if(!query) {
        return;
    }
    if(query->request) {
        query->request->refcount--;
        if(!query->request->refcount) {
            query_request_destroy(query->request);
        }
    }
    free(query->intf);
    free(query->subscriber);
    free(query->cl);
    argument_valueClear(&query->args);
    free(query);
}

static nemo_query_t* nemo_query_create(const char* intf, const char* subscriber, const char* cl, argument_value_list_t* args, uint32_t attributes, nemo_result_handler_t handler, void* userdata) {
    nemo_query_t* query = calloc(1, sizeof(nemo_query_t));
    if(!query) {
        SAH_TRACE_ERROR("nemo - calloc failed");
        goto error;
    }
    query->intf = strdup(intf);
    if(!query->intf) {
        SAH_TRACE_ERROR("nemo - strdup(%s) failed", intf);
        goto error;
    }
    query->subscriber = strdup(subscriber);
    if(!query->subscriber) {
        SAH_TRACE_ERROR("nemo - strdup(%s) failed", subscriber);
        goto error;
    }
    query->cl = strdup(cl);
    if(!query->cl) {
        SAH_TRACE_ERROR("nemo - strdup(%s) failed", cl);
        goto error;
    }
    argument_value_list_initialize(&query->args);
    argument_value_t* arg;
    for(arg = argument_valueFirstArgument(args); arg; arg = argument_valueNextArgument(arg)) {
        argument_valueAdd(&query->args, argument_valueName(arg), argument_value(arg));
    }
    query->attributes = attributes;
    query->handler = handler;
    query->userdata = userdata;
    query->to_destroy = false;
    return query;
error:
    nemo_query_destroy(query);
    return NULL;

}

nemo_query_t* nemo_openQuery(const char* intf, const char* subscriber, const char* cl, argument_value_list_t* args, uint32_t attributes, nemo_result_handler_t handler, void* userdata) {
    if(!intf) {
        intf = "lo";
    }
    if(!subscriber) {
        SAH_TRACE_ERROR("nemo - mandatory argument subscriber is NULL");
        return NULL;
    }
    if(!cl) {
        SAH_TRACE_ERROR("nemo - mandatory argument class is NULL");
        return NULL;
    }
    request_t* req = NULL;
    nemo_query_t* query = nemo_query_create(intf, subscriber, cl, args, attributes, handler, userdata);
    if(!query) {
        goto error;
    }
    req = request_create_executeFunction(nemo_intf2path(intf), "openQuery", request_common_path_key_notation | attributes);
    request_addCharParameter(req, "subscriber", subscriber);
    request_addCharParameter(req, "class", cl);
    argument_value_t* arg;
    while((arg = argument_valueTakeArgumentFirst(args))) {
        request_addArgument(req, arg);
    }
    if(!nemo_request_invoke(req)) {
        goto error;
    }
    uint32_t query_id = variant_uint32(nemo_request_returnValue(req));
    if(!query_id) {
        SAH_TRACE_ERROR("nemo - openQuery(intf=%s, subscriber=%s, class=%s) failed", intf, subscriber, cl);
        goto error;
    }
    request_destroy(req);
    req = NULL;
    query->request = query_request_find(query_id);
    if(!query->request) {
        query->request = query_request_create(intf, query_id);
    }
    if(!query->request) {
        goto error;
    }
    query->request->refcount++;

    // get initial result
    request_t* getResult = request_create_executeFunction(request_path(query->request->request), "getResult", request_common_path_key_notation);
    if(!nemo_request_invoke(getResult)) {
        request_destroy(getResult);
        goto error;
    }
    if(query->handler) {
        query->handler(nemo_request_returnValue(getResult), query->userdata);
    }
    request_destroy(getResult);

    llist_append(&queries, &query->it);
    return query;
error:
    if(req) {
        request_destroy(req);
    }
    nemo_query_destroy(query);
    return NULL;
}

void nemo_closeQuery(nemo_query_t* query) {
    if(!query) {
        return;
    }
    request_t* req = request_create_executeFunction(nemo_intf2path(query->intf), "closeQuery", request_common_path_key_notation | (query->attributes & request_function_args_by_name));
    request_addCharParameter(req, "subscriber", query->subscriber);
    request_addCharParameter(req, "class", query->cl);
    argument_value_t* arg;
    while((arg = argument_valueTakeArgumentFirst(&query->args))) {
        request_addArgument(req, arg);
    }
    nemo_request_invoke(req);
    request_destroy(req);
    if(handling) {
        query->to_destroy = true;
    } else {
        llist_iterator_take(&query->it);
        nemo_query_destroy(query);
    }
}

nemo_query_t* nemo_openQuery_isUp(const char* intf, const char* subscriber, const char* flag, const char* traverse, nemo_result_handler_t handler, void* userdata) {
    argument_value_list_t args;
    argument_value_list_initialize(&args);
    if(flag) {
        nemo_argument_valueAddChar(&args, "flag", flag);
    }
    if(traverse) {
        nemo_argument_valueAddChar(&args, "traverse", traverse);
    }
    nemo_query_t* query = nemo_openQuery(intf, subscriber, "isUp", &args, request_function_args_by_name, handler, userdata);
    argument_valueClear(&args);
    return query;
}
nemo_query_t* nemo_openQuery_hasFlag(const char* intf, const char* subscriber, const char* flag, const char* condition, const char* traverse, nemo_result_handler_t handler, void* userdata) {
    argument_value_list_t args;
    argument_value_list_initialize(&args);
    if(flag) {
        nemo_argument_valueAddChar(&args, "flag", flag);
    }
    if(condition) {
        nemo_argument_valueAddChar(&args, "condition", condition);
    }
    if(traverse) {
        nemo_argument_valueAddChar(&args, "traverse", traverse);
    }
    nemo_query_t* query = nemo_openQuery(intf, subscriber, "hasFlag", &args, request_function_args_by_name, handler, userdata);
    argument_valueClear(&args);
    return query;
}
nemo_query_t* nemo_openQuery_isLinkedTo(const char* intf, const char* subscriber, const char* target, const char* traverse, nemo_result_handler_t handler, void* userdata) {
    argument_value_list_t args;
    argument_value_list_initialize(&args);
    if(target) {
        nemo_argument_valueAddChar(&args, "target", target);
    }
    if(traverse) {
        nemo_argument_valueAddChar(&args, "traverse", traverse);
    }
    nemo_query_t* query = nemo_openQuery(intf, subscriber, "isLinkedTo", &args, request_function_args_by_name, handler, userdata);
    argument_valueClear(&args);
    return query;
}
nemo_query_t* nemo_openQuery_getIntfs(const char* intf, const char* subscriber, const char* flag, const char* traverse, nemo_result_handler_t handler, void* userdata) {
    argument_value_list_t args;
    argument_value_list_initialize(&args);
    if(flag) {
        nemo_argument_valueAddChar(&args, "flag", flag);
    }
    if(traverse) {
        nemo_argument_valueAddChar(&args, "traverse", traverse);
    }
    nemo_query_t* query = nemo_openQuery(intf, subscriber, "getIntfs", &args, request_function_args_by_name, handler, userdata);
    argument_valueClear(&args);
    return query;
}
nemo_query_t* nemo_openQuery_luckyIntf(const char* intf, const char* subscriber, const char* flag, const char* traverse, nemo_result_handler_t handler, void* userdata) {
    argument_value_list_t args;
    argument_value_list_initialize(&args);
    if(flag) {
        nemo_argument_valueAddChar(&args, "flag", flag);
    }
    if(traverse) {
        nemo_argument_valueAddChar(&args, "traverse", traverse);
    }
    nemo_query_t* query = nemo_openQuery(intf, subscriber, "luckyIntf", &args, request_function_args_by_name, handler, userdata);
    argument_valueClear(&args);
    return query;
}
nemo_query_t* nemo_openQuery_getFirstParameter(const char* intf, const char* subscriber, const char* name, const char* flag, const char* traverse, nemo_result_handler_t handler, void* userdata) {
    argument_value_list_t args;
    argument_value_list_initialize(&args);
    if(name) {
        nemo_argument_valueAddChar(&args, "name", name);
    }
    if(flag) {
        nemo_argument_valueAddChar(&args, "flag", flag);
    }
    if(traverse) {
        nemo_argument_valueAddChar(&args, "traverse", traverse);
    }
    nemo_query_t* query = nemo_openQuery(intf, subscriber, "getFirstParameter", &args, request_function_args_by_name, handler, userdata);
    argument_valueClear(&args);
    return query;
}
nemo_query_t* nemo_openQuery_getParameters(const char* intf, const char* subscriber, const char* name, const char* flag, const char* traverse, nemo_result_handler_t handler, void* userdata) {
    argument_value_list_t args;
    argument_value_list_initialize(&args);
    if(name) {
        nemo_argument_valueAddChar(&args, "name", name);
    }
    if(flag) {
        nemo_argument_valueAddChar(&args, "flag", flag);
    }
    if(traverse) {
        nemo_argument_valueAddChar(&args, "traverse", traverse);
    }
    nemo_query_t* query = nemo_openQuery(intf, subscriber, "getParameters", &args, request_function_args_by_name, handler, userdata);
    argument_valueClear(&args);
    return query;
}
