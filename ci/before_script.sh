#!/bin/bash

mkdir -p $CI_PROJECT_DIR/output/deps
mkdir -p $CI_PROJECT_DIR/output/staging
mkdir -p $CI_PROJECT_DIR/output/package

  # move generic/global component config files to staging directory
cp -r /root/staging/* $CI_PROJECT_DIR/output/staging

  # set version
if [ -z $CI_COMMIT_TAG ]; then
  export PACKAGE_VERSION="9.9.9";
else
  version=$(echo $CI_COMMIT_TAG | cut -d_ -f2);
  export PACKAGE_VERSION="${version:1}";
fi

if [ -z $CI_COMMIT_TAG ]; then
  export PACKAGE_DIR="$CI_COMMIT_REF_NAME";
else
  export PACKAGE_DIR=$(echo $CI_COMMIT_TAG | cut -d_ -f1);
fi
