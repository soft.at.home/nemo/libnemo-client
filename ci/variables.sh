#!/bin/bash
# NOTE: this should be synced with the `variables` section in `.gitlab-ci.yml`

export CI_PROJECT_DIR=$(pwd) # must be run from parent path.

export COMPONENT_DEPS_DIR=master
export COMPONENT_OPENSSL_DIR=gen_1.0.2k
# Package dependencies
export COMPONENT_DEPS="$COMPONENT_DEPS_DIR/sah-lib-sahtrace-dev $COMPONENT_OPENSSL_DIR/sah-lib-openssl-dev $COMPONENT_DEPS_DIR/sah-lib-usermngt-dev $COMPONENT_DEPS_DIR/sah-lib-pcb-dev"
# Component settings
export PACKAGE_NAME=sah-lib-nemo-client-dev
# redefine staging directory and packaging directory
export STAGINGDIR=$CI_PROJECT_DIR/output/staging
export PACKAGEDIR=$CI_PROJECT_DIR/output/package
# Ci package storage
export PACKAGE_STORAGE=packages@packages.be.softathome.com

