#!/bin/bash

# gitlab ci currently does not support globbing for artifacts
#
# we would like to write in the .gitlab-ci.yml
#
# artifacts:
#   reports:
#     junit: **/unit.xml
# 
# since this does currently not work, we add a workaround shell script
# for it.

# unittest + generate xml reports (junit format)
make -f Component.mk report
reportresult=$?
echo "Test result = ${reportresult}"

cd $CI_PROJECT_DIR/test
for i in $(find . -name "*.xml"); do
	path=$(dirname ${i})
	subdir=$(basename ${path})
	echo "cp ${i} ${subdir}.xml"
	cp ${i} ${subdir}.xml
	echo "junit2html ${subdir}.xml ${subdir}.html"
	junit2html ${subdir}.xml ${subdir}.html
done

exit $reportresult
