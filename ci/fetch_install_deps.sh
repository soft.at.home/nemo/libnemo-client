#!/bin/bash

mkdir -p $CI_PROJECT_DIR/output/deps
for i in $COMPONENT_DEPS; do
    echo "Fetching ${i}.deb"
    scp -o StrictHostKeyChecking=no $PACKAGE_STORAGE:/home/packages/ci/${i}.deb $CI_PROJECT_DIR/output/deps/
done
dpkg -i $CI_PROJECT_DIR/output/deps/*.deb
