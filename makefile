-include $(CONFIGDIR)/components.config

export INSTALL ?= install
export PKG_CONFIG_LIBDIR ?= /usr/lib/pkgconfig
export BINDIR ?= /usr/bin
export LIBDIR ?= /usr/lib
export SLIBDIR ?= /usr/lib
export LUALIBDIR ?= /usr/lib/lua
export INCLUDEDIR ?= /usr/include
export INITDIR ?= /etc/init.d
export ACLDIR ?= /etc/acl
export DOCDIR ?= $(D)/usr/share/doc/libnemo-client
export PROCMONDIR ?= /usr/lib/processmonitor/scripts
export RESETDIR ?= /etc/reset
export MACHINE ?= $(shell $(CC) -dumpmachine)

export COMPONENT = libnemo-client

compile:
	$(MAKE) -C src all

clean:
	$(MAKE) -C src clean

install:
	$(INSTALL) -D -p -m 0755 src/libnemo_client.so $(D)/lib/libnemo_client.so.$(PV)
	ln -sfr $(D)/lib/libnemo_client.so.$(PV) $(D)/lib/libnemo_client.so
	$(INSTALL) -d -m 0755 $(D)/$(INCLUDEDIR)/nemo/client
	$(INSTALL) -D -p -m 0644 include/nemo/client/*.h $(D)$(INCLUDEDIR)/nemo/client/
	$(INSTALL) -d -m 0755 $(D)/$(INCLUDEDIR)/nemo
	$(INSTALL) -D -p -m 0644 include/nemo/*.h $(D)$(INCLUDEDIR)/nemo/
	$(INSTALL) -D -p -m 0644 include/netdev/common_api.h $(D)$(INCLUDEDIR)/netdev/common_api.h
	$(INSTALL) -D -p -m 0644 pkgconfig/nemo_client.pc $(PKG_CONFIG_LIBDIR)/nemo_client.pc
	$(INSTALL) -D -p -m 0644 pkgconfig/netdev_client.pc $(PKG_CONFIG_LIBDIR)/netdev_client.pc

.PHONY: compile clean install
