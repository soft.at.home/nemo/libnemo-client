/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#ifndef __NEMO_CLIENT_H__
#define __NEMO_CLIENT_H__

/**
   @defgroup client client.h - Client Library
   @{

   @brief
   The NeMo Client Library is a collection of utilities to facilitate out-of-process data model access to NeMo.

   @details
   The NeMo Client Library contains an implementation of the NeMo Common API (see @ref common_api), to be used by client applications and
   data model plugins. On top of that, the libary contains some other utilities that are not available in the NeMo Core Libary as well.

   Before using the library, please read the following important preconditions:
   - The application needs to have a PCB event loop. The library has functionality that registers callbacks to the PCB core library
   and it relies on getting called by the PCB event loop.
   - The library has no support for multi-threading whatsoever.
   - The library contains blocking I/O. The API functions send one or more requests to the NeMo core process and wait for the reply.

   The functionality of the library is backed by PCB. In fact, the library is only a thin layer on top of PCB specifically for
   NeMo data model access. So if one of the above preconditions do not hold for your application, consider having a look at the source
   code and write your own version of it suited for your environment.

   To use the NeMo Client Library, applications shall:
   - Include <nemo/client.h>.
   - Add the package nemo_client to the pkg-config command line, e.g. link with `pkg-config --libs nemo_client`.
   - Depend on sah_lib_nemo in their Component.dep and on SAH_LIB_NEMO in their Component.in.

   @author Dries De Winter <dries.dewinter@softathome.com>
 */

#include <stdbool.h>
#include <stdint.h>

#include <nemo/common_api.h>
#include <nemo/client/csi.h>
#include <nemo/client/dhcp.h>
#include <nemo/client/netdev.h>
#include <nemo/client/wlan.h>
#include <nemo/client/ra.h>

/**
   @brief
   Initialize the NeMo Client Library.

   @details
   A reference to the PCB instance needs to be passed to this function and a peer through which the NeMo core process is reachable
   can be given to this initializer optionally.
   These references are stored in global variables and used by almost all other functions of the client library, so this
   function MUST be called BEFORE any other function of the library is called. This also implies that these objects MUST NOT be
   destroyed until the library's references to them are erased. See also nemo_client_cleanup().

   If no peer is given, the client library tries to setup its own connection using the following list of methods in descending order
   of priority:
   -# Connect to the PCB URI given by the environmental variable NEMO_URI.
   -# Connect to pcb://ipc:[/var/run/nemo] which is the Unix socket where NeMo is listening on by default.
   -# Connect to the PCB URI given by the environmental variable PCB_SYS_BUS. If a direct connection to NeMo is not possbile, fall
   back to the PCB system bus. Notice that this method comes with a performance penalty.
   -# Connect to pcb://ipc:[/var/run/pcb_sys] which is the Unix socket where PCB system bus is listening on by default.

   All syslog messages of the library are logged using sahtrace zone "nemo". If this zone is not defined when calling
   nemo_client_initialize(), it's created automatically with default log level 300 (TRACE_LEVEL_NOTICE). This can be overruled by
   defining the zone and the corresponding log level before calling nemo_client_initialize().

   The library keeps a global reference counter which is incremented each time nemo_client_initialize() is called and decremented each
   time nemo_client_cleanup() is called. If the reference counter is non-zero when this function is entered, it does nothing. Caution:
   if this function failed the first time it was called, it will always fail until the reference counter reaches zero again because
   incrementing the reference counter does not imply retrying to connect if it failed previously.

   @param pcb The PCB instance
   @param peer The peer through which the NeMo core process is reachable or NULL to let the library set up its own connection.
   @return True if the connection is set up successfully. False if no of the connection methods succeeded. Notice that if the caller
        provided a non-NULL peer, this function always returns true.
 */
bool nemo_client_initialize(pcb_t* pcb, peer_info_t* peer);

/**
   @brief
   Cleanup the NeMo Client Library.

   @details
   This erases the references to the PCB instance and the peer passed to nemo_client_initialize().
   Without these references, the library is not functional, so this function MUST be called AFTER the last call to any other function
   of the library, but BEFORE the objects referenced by pcb and peer are destroyed.

   If the connection to NeMo was set up by the library itself, using one the methods described in the documentation of
   nemo_client_initialize(), this function will close the connection. If the caller provided a peer to the library, the peer is left
   untouched by this function.

   The library keeps a global reference counter which is incremented each time nemo_client_initialize() is called and decremented each
   time nemo_client_cleanup() is called. Only if the reference counter reaches zero when calling this function, this function
   does something effectively. Caution: this means that the references to the PCB instance and the peer passed to
   the FIRST call to nemo_client_initialize() are kept until the LAST call to nemo_client_cleanup().
 */
void nemo_client_cleanup();

/**
   @brief
   Returns the PCB instance used by the library.

   @return the PCB instance used by the library, passed to nemo_client_initialize().
 */
pcb_t* nemo_client_pcb();

/**
   @brief
   Returns the peer used by the library.

   @return the peer used by the library, passed to or set up by nemo_client_initialize().
 */
peer_info_t* nemo_client_peer();

/**
   @}
 */

#endif
