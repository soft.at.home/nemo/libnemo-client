/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#ifndef __NEMO_CLIENT_NETDEV_H__
#define __NEMO_CLIENT_NETDEV_H__

/**
   @ingroup client
   @defgroup netdev netdev.h - NetDev API
   @{

   @brief
   NetDev API is an extension on top of the Common API (see @ref common_api) to facilitate out-of-process access to the NetDev API MIB.
 */

#include <stdbool.h>
#include <stdint.h>

#include <pcb/utils.h>
#include <pcb/core.h>

#include <nemo/common_api.h>

/**
   @brief
   Get all active Addr objects (Status is bound or dynamic) that occur in the traverse tree.

   @param intf Starting point of the traverse tree. Default is "lo".
   @param flag Only Addrs for which this flag expression evaluates to true are returned.
            An Addr's flag set is defined by the union of the Flags parameter, the Scope parameter and the address family
            ("ipv4" or "ipv6"), so you could call nemo_getAddrs(NULL, "ipv6 && global", "all") to get all global IPv6 addresses
            for example. Default is empty.
   @param traverse Traverse mode to build the traverse tree. Default is "down".
   @return A list of object maps. An object map is a map that maps parameter names to parameter values.

   @warning
   The returned variant_list is allocated on the heap, so it MUST be cleaned up AND freed by
   caller of the function with variant_list_cleanup(), followed by free().
 */
variant_list_t* nemo_getAddrs(const char* intf, const char* flag, const char* traverse);

/**
   @brief
   Get the first active Addr (Status is bound or dynamic) that occurs in the traverse tree.

   @param intf Starting point of the traverse tree. Default is "lo".
   @param flag Only Addrs for which this flag expression evaluates to true are considered.
            An Addr's flag set is defined by the union of the Flags parameter, the Scope parameter and the address family
            ("ipv4" or "ipv6"), so you could call nemo_luckyAddr(NULL, "ipv6 && global", "all") to get any global IPv6
            address for example. Default is empty.
   @param traverse Traverse mode to build the traverse tree. Default is "down".
   @return An object map. An object map is a map that maps parameter names to parameter values.

   @warning
   The returned variant_map is allocated on the heap, so it MUST be cleaned up AND freed by
   caller of the function with variant_map_cleanup(), followed by free().
 */
variant_map_t* nemo_luckyAddr(const char* intf, const char* flag, const char* traverse);

/**
   @brief
   Get the only the Address parameter of the first active Addr (Status is bound or dynamic) that occurs in the traverse tree.

   @param intf Starting point of the traverse tree. Default is "lo".
   @param flag Only Addrs for which this flag expression evaluates to true are considered.
            An Addr's flag set is defined by the union of the Flags parameter, the Scope parameter and the address family
            ("ipv4" or "ipv6"), so you could call nemo_luckyAddrAddress(NULL, "ipv6 && global", "all") to get any global IPv6
            address for example. Default is empty.
   @param traverse Traverse mode to build the traverse tree. Default is "down".
   @return An IP-address in standard notation.

   @warning
   The returned string is allocated on the heap, so it MUST be freed by
   caller of the function with free().
 */
char* nemo_luckyAddrAddress(const char* intf, const char* flag, const char* traverse);

/**
   @brief
   Set one or more flags on all Intfs in the traverse tree.

   @param intf Starting point of the traverse tree. Default is "lo".
   @param flag The flagset holding all flags to set. Default is empty.
   @param traverse Traverse mode to build the traverse tree. Default is "down".
 */
void nemo_setNetDevFlag(const char* intf, const char* flag, const char* traverse);

/**
   @brief
   Clear one or more flags on all Intfs in the traverse tree.

   @param intf Starting point of the traverse tree. Default is "lo".
   @param flag The flagset holding all flags to set. Default is empty.
   @param traverse Traverse mode to build the traverse tree. Default is "down".
 */
void nemo_clearNetDevFlag(const char* intf, const char* flag, const char* traverse);

/**
   @brief
   nemo_openQuery() wrapper for querying the function nemo_getAddrs().

   @details
   nemo_openQuery() wrapper for querying the function nemo_getAddrs().

   @param intf see nemo_openQuery() and/or nemo_getAddrs()
   @param subscriber see nemo_openQuery()
   @param flag see nemo_getAddrs()
   @param traverse see nemo_getAddrs()
   @param handler see nemo_openQuery()
   @param userdata see nemo_openQuery()
   @return see nemo_openQuery()
 */
nemo_query_t* nemo_openQuery_getAddrs(const char* intf, const char* subscriber, const char* flag,
                                      const char* traverse, nemo_result_handler_t handler, void* userdata);

/**
   @brief
   nemo_openQuery() wrapper for querying the function nemo_luckyAddr().

   @param intf see nemo_openQuery() and/or nemo_luckyAddr()
   @param subscriber see nemo_openQuery()
   @param flag see nemo_luckyAddr()
   @param traverse see nemo_luckyAddr()
   @param handler see nemo_openQuery()
   @param userdata see nemo_openQuery()
   @return see nemo_openQuery()
 */
nemo_query_t* nemo_openQuery_luckyAddr(const char* intf, const char* subscriber, const char* flag,
                                       const char* traverse, nemo_result_handler_t handler, void* userdata);

/**
   @brief
   nemo_openQuery() wrapper for querying the function nemo_luckyAddrAddress().

   @param intf see nemo_openQuery() and/or nemo_luckyAddrAddress()
   @param subscriber see nemo_openQuery()
   @param flag see nemo_luckyAddrAddress()
   @param traverse see nemo_luckyAddrAddress()
   @param handler see nemo_openQuery()
   @param userdata see nemo_openQuery()
   @return see nemo_openQuery()
 */
nemo_query_t* nemo_openQuery_luckyAddrAddress(const char* intf, const char* subscriber, const char* flag,
                                              const char* traverse, nemo_result_handler_t handler, void* userdata);

/**
   @}
 */

#endif
