/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#ifndef __NEMO_CLIENT_RA_H__
#define __NEMO_CLIENT_RA_H__

/**
   @ingroup client
   @defgroup ra ra.h - RA API
   @{

   @brief
   RA API is an extension on top of the Common API (see @ref common_api) to facilitate out-of-process access to the RA API MIB.
 */

#include <stdbool.h>
#include <stdint.h>

#include <pcb/utils.h>

#include <nemo/common_api.h>

/**
   @brief
   Finds and parses all option values of type 'tag' of all router advertisements received on any IPv6 interface in the traverse tree.

   @param intf Starting point of the traverse tree. Default is "lo".
   @param tag The option tag or type.
   @param traverse Traverse mode to build the traverse tree. Default is "down".

   @return
   A list of received router advertisement options. Each option is a map containing the following fields:
   - Intf: The NeMo Intf name of the interface on which the option was found.
   - Router: The source IP-address of the router advertisement advertising this option.
   - LastAdvertisement: Timestamp of the last received router advertisement containing this option. This timestamp is expressed in seconds since power on.
   - Value: A structured variant holding the option value. Its exact structure depends on the tag argument: each option
   has its particular encoding scheme. Example: if the tag equals 3 (Prefix Information Option, see RFC 4861), the return value is
   a variant map holding the fields PrefixLength, L, A, ValidLifetime, PreferredLifetime and Prefix. Supported options are: 1, 2, 3, 5, 25 and 31. If an unsupported option tag is given as input argument, a default encoding scheme is applied. This encoding scheme returns an ASCII-string if the option value contains valid ASCII-characters only or the raw hexbinary prefixed with "0x" otherwise.

   @warning
   The returned variant is allocated on the heap, so it MUST be cleaned up AND freed by
   caller of the function with variant_cleanup(), followed by free().
 */
variant_t* nemo_getRAOptions(const char* intf, uint8_t tag, const char* traverse);

/**
   @brief
   Find all advertising routers on any IPv6 interface in the traverse tree and return some information about their advertisements.

   @param intf Starting point of the traverse tree. Default is "lo".
   @param traverse Traverse mode to build the traverse tree. Default is "down".

   @return
   A list of routers of which a router advertisement was received. Each router is a map containing the following fields:
   - Intf: The NeMo Intf name of the interface on which the router was found.
   - Router: The source IP-address of the router's router advertisements.
   - LastAdvertisement: Timestamp of the last received router advertisement. This timestamp is expressed in seconds since power on.
   - CurHopLimit
   - Managed
   - Other
   - RouterLifetime
   - ReachableTime
   - RetransTimer
   See RFC 4861 for more info on these fields.

   @warning
   The returned variant is allocated on the heap, so it MUST be cleaned up AND freed by
   caller of the function with variant_cleanup(), followed by free().
 */
variant_t* nemo_getRARouters(const char* intf, const char* traverse);

/**
   @brief
   nemo_openQuery() wrapper for querying the function nemo_getRAOptions().

   @param intf see nemo_openQuery() and/or nemo_getRAOptions()
   @param subscriber see nemo_openQuery()
   @param tag see nemo_getRAOptions()
   @param traverse see nemo_getRAOptions()
   @param handler see nemo_openQuery()
   @param userdata see nemo_openQuery()
   @return see nemo_openQuery()
 */
nemo_query_t* nemo_openQuery_getRAOptions(const char* intf, const char* subscriber, uint8_t tag,
                                          const char* traverse, nemo_result_handler_t handler, void* userdata);

/**
   @brief
   nemo_openQuery() wrapper for querying the function nemo_getRARouters().

   @param intf see nemo_openQuery() and/or nemo_getRARouters()
   @param subscriber see nemo_openQuery()
   @param tag see nemo_getRARouters()
   @param traverse see nemo_getRARouters()
   @param handler see nemo_openQuery()
   @param userdata see nemo_openQuery()
   @return see nemo_openQuery()
 */
nemo_query_t* nemo_openQuery_getRARouters(const char* intf, const char* subscriber,
                                          const char* traverse, nemo_result_handler_t handler, void* userdata);

/**
   @}
 */

#endif

