/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#ifndef __NEMO_CLIENT_WLAN_H__
#define __NEMO_CLIENT_WLAN_H__

/**
   @ingroup client
   @defgroup wlan wlan.h - WLANConfig
   @{

   @brief
   WLAN is an extension on top of the Common API (see @ref common_api) to facilitate out-of-process access to the WLAN Config MIB.
 */

#include <pcb/utils.h>

/**
   @brief
   This function is a wrapper for nemo_setMIBs() intended for wlan reconfigurations.

   @details
   Except for calling nemo_setMIBs(), it also calls
   the wland's edit() before setting the MIBs and commit() after setting the MIBs for all affected Radios to guarantee atomic
   reconfiguration, minimizing the performance penalty of it. See also documentation of WLAN daemon.

   @param intf Dummy Intf. It does not affect the behavior of the function but it must exist. Default is "lo".
   @param mibs A variant_map in the same format as the return value of nemo_getMIBs() [ i.e.
                A variant_map that maps MIB names to MIB maps. A MIB map is a variant_map that maps Intf names to object maps.
                An object map is a variant_map that maps parameter names to parameter values (variants) and child object names
                (including instances of templates) to object maps recursively.
            ]. However, the MIB maps don't have to be complete. Parameters and objects of the MIB that are missing (or discarded
            from the return value of nemo_getMIBs()) are left untouched, except for the instances of template objects:
            for each template object included by a MIB map, any unexisting instance included by the MIB map is created implicitly
            and any existing instance not included by MIB map is deleted implicitly. Mandatory.
 */
void nemo_setWLANConfig(const char* intf, variant_map_t* mibs);

/**
   @}
 */

#endif
