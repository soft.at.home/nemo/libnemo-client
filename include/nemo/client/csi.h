/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#ifndef __NEMO_CLIENT_CSI_H__
#define __NEMO_CLIENT_CSI_H__

/**
   @ingroup client
   @defgroup csi csi.h - Client-Side Implementations
   @{

   @brief
   Utility for implementing NeMo data model functions in a client process.

   @details
   The data model of NeMo supports client-side implementations of functions through the functions csiRegister(), csiUnregister() and
   csiFinish() and the notifications csiExec[102] and csiCancel[103]. For detailed information on how this works via the data model,
   please refer to the data model documentation, but basically it comes down to this:
   -# Client A calls NeMo data model function f() for which client B registered a CSI or Client-Side Implementation.
   -# NeMo notifies client B that f() has been called while keeping the function call in busy state.
   -# Client B calls csiFinish() to tell NeMo that function call is finished.
   -# NeMo terminates the outstanding function call and sends the reply to client A.

   This module has an intuitive API so this kind of data model function implementations can be provided easily. It is more or less
   similar to the function call API of PCB, with emphasis on "more or less".
 */

#include <stdbool.h>
#include <stdint.h>

#include <pcb/utils.h>
#include <pcb/core.h>

/**
   @brief
   Opaque descriptor of outstanding function calls.

   @details
   This kind of descriptor holds all properties of a function call, including:
   - The Intf on which the function was invoked.
   - The name or spec of the invoked function.
   - The list of input/output arguments.
   - The function return value.
   - The list of error replies.
   .
   They can be accessed with appropriate accessor functions.

   @warning
   Its memory management is rather complicated. Refer to the documentation of @ref nemo_exec_handler_t, @ref nemo_cancel_handler_t and
   nemo_csicall_finish() for this. However, the general rule is that only when and whether to call nemo_csicall_finish() needs to
   be considered very carefully.
 */
typedef struct _nemo_csicall nemo_csicall_t;

/**
   @brief
   CSI invokation handler type.

   @param csicall A pointer to an opaque descriptor of the CSI call.
   @return
        - function_exec_done to tell NeMo that the function call finished successfully.
        - function_exec_error to tell NeMo that an error occurred.
        - function_exec_busy to tell NeMo that the function call didn't finish yet. You can finish it later by calling
          nemo_csicall_finish(), if it didn't get canceled by then. This allows to implement functions asynchronously.

   @warning
   The descriptor is allocated on the heap and if function_exec_done or function_exec_error is returned, it is cleaned up and freed
   by the library, so please don't record the csicall pointer in that case. If function_exec_busy is returned on the other hand,
   the csicall pointer must be recorded because it needs to be passed later on to nemo_csicall_finish().

   @see nemo_csicall_t.
   @see nemo_csiRegister().
 */
typedef function_exec_state_t (* nemo_exec_handler_t)(nemo_csicall_t* csicall);

/**
   @brief
   CSI cancel handler type.

   @param csicall A pointer to an opaque descriptor of the CSI call.

   @warning
   The descriptor is allocated on the heap and it is cleaned up and freed by the library right after calling the cancel handler,
   so please erase all your references to the descriptor in this handler because they will become invalid.
   This implies that nemo_csicall_finish() can not be called anymore after the cancel handler was invoked.

   @par
   @warning
   The cancel handler can only be called after the exec handler returned function_exec_busy, but before nemo_csicall_finish() was
   called, so:
   - Installing a cancel handler knowing that the exec handler will never return function_exec_busy is pointless.
   - BUT! If there is any possibility that the exec handler returns function_exec_busy, you must provide a cancel handler, because
   in order to finish the csicall later, you probably recorded the CSI call descriptor somewhere which becomes invalid after it
   gets canceled. So you must provide a cancel handler that erases all references to the CSI call descriptor in that case.

   @see nemo_csicall_t.
   @see nemo_csiRegister().
 */
typedef void (* nemo_cancel_handler_t)(nemo_csicall_t* csicall);

/**
   @brief
   Register a client-side implementation of a data model function.

   @param intf The Intf on which a client-side implementation needs to be registered.
   @param func The name or spec of the function.
   @param exec The invokation handler. This is the actual function implementation. See also @ref nemo_exec_handler_t.
   @param cancel The cancellation handler. This callback function is called when an asynchronously implemented
              function gets cancelled before it was finished. See also @ref nemo_exec_handler_t.
   @param userdata A void pointer to be passed to the invokation handler (through the CSI call descriptor).
 */
void nemo_csiRegister(const char* intf, const char* func, nemo_exec_handler_t exec, nemo_cancel_handler_t cancel, void* userdata);

/**
   @brief
   Unregister a client-side implementation of a data model function.

   @param intf The Intf on which a client-side implementation needs to be unregistered.
   @param func The name or spec of the function.

   @warning
   This function implicitly destroys all derived CSI call descriptors, so please watch your pointers!
 */
void nemo_csiUnregister(const char* intf, const char* func);

/**
   @brief
   Retrieve the Intf on which a CSI call was invoked.

   @param csicall A CSI call descriptor.
   @return The name of the Intf.
 */
const char* nemo_csicall_intf(nemo_csicall_t* csicall);

/**
   @brief
   Retrieve the name or spec of the invoked function.

   @param csicall A CSI call descriptor.
   @return The name or spec of the invoked function.
 */
const char* nemo_csicall_func(nemo_csicall_t* csicall);

/**
   @brief
   Retrieve the userdata void pointer installed by nemo_csiRegister() or nemo_csicall_setUserdata().

   @param csicall A CSI call descriptor.
   @return The void pointer installed by nemo_csiRegister() or nemo_csicall_setUserdata().
 */
void* nemo_csicall_userdata(nemo_csicall_t* csicall);

/**
   @brief
   Set the userdata void pointer of a CSI call descriptor.

   @details
   The userdata void pointer of a CSI call descriptor points by default to the one passed to nemo_csiRegister(), but in order to
   correctly implement a cancel handler, it could be useful or even mandatory to replace this CSI-specific void pointer with a
   call-specific void pointer in the exec handler.

   @param csicall A CSI call descriptor.
   @param userdata A void pointer pointing to the new userdata.
 */
void nemo_csicall_setUserdata(nemo_csicall_t* csicall, void* userdata);

/**
   @brief
   Get the input/output argument list of a CSI call.

   @details
   This argument list represents both input arguments and output arguments. If this list is left untouched by the caller,
   the output argument list of the CSI call will equal the input argument list. Like in PCB, function implementations should actually
   "consume" their input arguments, i.e. take them from the list and add output arguments to the list.

   @param csicall A CSI call descriptor.
   @return The input/output argument list.
 */
argument_value_list_t* nemo_csicall_arguments(nemo_csicall_t* csicall);

/**
   @brief
   Get the attributes of a CSI call.

   @details
   Currently the only supported attribute is
   request_function_args_by_name, defined by <pcb/core/request.h>, indicating that the arguments should be
   processed by name lookup rather than by order.

   @param csicall A CSI call descriptor.
   @return The bitwise OR of all attributes.
 */
uint32_t nemo_csicall_attributes(nemo_csicall_t* csicall);

/**
   @brief
   Get the return value of a CSI call.

   @param csicall A CSI call descriptor.
   @return A writable variant pointer, implementations should store the function return value into this variant.
 */
variant_t* nemo_csicall_returnValue(nemo_csicall_t* csicall);

/**
   @brief
   Add an error reply to a CSI call.

   @param csicall A CSI call descriptor.
   @param code An error code. This may be interpreted as a pcb_error_t. It is permitted to deviate from the by PCB predefined
            constants, but try to avoid providing a valid pcb_error_t while meaning something else.
   @param description Human readable description of the error code.
   @param info Extra information regarding the error, for example the data value that caused the error.
 */
void nemo_csicall_addError(nemo_csicall_t* csicall, uint32_t code, const char* description, const char* info);

/**
   @brief
   Finish an outstanding CSI call.

   @details
   This function tells NeMo that the function call is finished. This will trigger sending the reply to the original data model function
   caller.

   @param csicall A CSI call descriptor.
   @param state State of the function call. Should be either function_exec_done or either function_exec_error.

   @warning
   If the exec handler returns function_exec_error or function_exec_done, the function call is finished automatically by the library,
   so in that case this function MUST NOT be called. Also when a CSI call has been canceled, i.e. cancel handler has been called,
   this function MUST NOT be called. So shortly, nemo_csicall_finish() should only be called after the exec handler returned
   function_exec_busy, but before the cancel handler was called.

   @par
   @warning
   Except for telling NeMo that the CSI call finished, this function also releases resources used internally by the library,
   including the CSI call descriptor. So after calling this function, the caller should erase all references to the provided
   descriptor or at least never use it again.
 */
void nemo_csicall_finish(nemo_csicall_t* csicall, function_exec_state_t state);

/**
   @}
 */

#endif

