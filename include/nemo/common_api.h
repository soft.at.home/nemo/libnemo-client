/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#ifndef __NEMO_COMMON_API_H__
#define __NEMO_COMMON_API_H__

/**
   @defgroup common_api common_api.h - Common API
   @{

   @brief
   The NeMo Common API is an C-style version of a subset of the NeMo Data Model API.

   @details
   This API has two implementations: one in the
   NeMo Client Library (see @ref client) and one in the NeMo Core Library (see @ref core) to maximize the availability of the API.
   Applications can use the NeMo Client Library, modules running inside the NeMo core process can use the NeMo Core Library and
   scripts can use the data model API directly (shell scripts via pcb_cli, javascript via HTTP serializer).

   The documentation of the API included herein is restricted to the details per function. Basic knowledge about NeMo is required
   to be able to read this. More high level documentation on NeMo can be found at
   http://wiki.be.softathome.com/mediawiki/index.php/Doc:NeMo_Data_Model#Architecture . On that page, some terminology
   definitions can be found as well, which are repeated here for completeness:
   - A <b>flag set</b> is a space separated list of flag names. Example: "enabled up".
   - A <b>flag expression</b> is a string in which flag names are combined with the logical operators &&, || and !.
   Subexpressions may be grouped with parentheses.
   The empty string is also a valid flag expression and it evaluates to true by definition. Example: "enabled && up".
   - Starting at a given Intf, the network stack dependency graph can be traversed in different ways. There are six predefined
   <b>traverse modes</b>:
   - <b>this</b> consider only the starting Intf.
   - <b>down</b> consider the entire closure formed by recursively following the LLIntf references.
   - <b>up</b> consider the entire closure formed by recursively following the ULIntf referenes.
   - <b>down exclusive</b> the same as down, but exclude the starting Intf.
   - <b>up exclusive</b> the same as up, but exclude the starting Intf.
   - <b>one level down</b> consider only direct LLIntfs.
   - <b>one level up</b> consider only direct ULIntfs.
   - <b>all</b> consider all Intfs.
   .
   The resulting structured set of Intfs is called the <b>traverse tree</b>.
   Example: if you apply the traverse mode "down" on Intf eth1 which has LLIntfs swport1, swport2 and swport3,
   the traverse tree will consist of eth1, swport1, swport2 and swport3.
   - Some data model functions accept a parameter and/or a function name as input argument. By extension, they may also accept a
   <b>parameter spec</b> and/or <b>function spec</b> as input argument. A parameter/function spec is the concatentation of
   the dot-separated key path relative to a NeMo Intf instance and the parameter/function name, separated by an extra dot.
   Example: the parameter spec "ReqOption.3.Value" refers to the parameter Value held by the object NeMo.Intf.{i}.ReqOption.3.

   @remarks
   In PCB, data model functions have mandatory arguments and optional arguments. The NeMo data model documentation clearly
   states which arguments are mandatory and for optional arguments, there is always a default value. More precisely, if an optional
   argument is omitted, the function behaves the exact same way as if the so called default value was provided.
   In C, optional arguments do not exist. However, passing a NULL pointer is handled the same way by the C style API
   implementation as not passing the argument at all by the data model API implementation. So for all pointer arguments of all
   functions, it is specified whether the argument is mandatory and if not, a default value is specified. Being mandatory means that
   NULL pointers are not accepted and if it's not mandatory, a NULL pointer will be handled the exact same way as the default value.

   @author Dries De Winter <dries.dewinter@softathome.com>
 */

#include <stdbool.h>
#include <stdint.h>

#include <pcb/utils.h>
#include <pcb/core.h>

/**
   @brief
   Query result handler type.

   @param result The current query result.
   @param userdata Userdata pointer provided to nemo_openQuery().
   @see nemo_openQuery().
 */
typedef void (* nemo_result_handler_t)(const variant_t* result, void* userdata);

/**
   @brief
   Opaque handle for an opened query.

   @details
   This type of object is returned by nemo_openQuery() and needs to be passed to nemo_closeQuery() exactly once.
   @see nemo_openQuery()
   @see nemo_closeQuery()
 */
typedef struct _nemo_query nemo_query_t;

extern const char* nemo_traverse_this;           /**< String constant holding the traverse mode "this".*/
extern const char* nemo_traverse_down;           /**< String constant holding the traverse mode "down".*/
extern const char* nemo_traverse_up;             /**< String constant holding the traverse mode "up".*/
extern const char* nemo_traverse_down_exclusive; /**< String constant holding the traverse mode "down exclusive".*/
extern const char* nemo_traverse_up_exclusive;   /**< String constant holding the traverse mode "up exclusive".*/
extern const char* nemo_traverse_one_level_down; /**< String constant holding the traverse mode "one level down".*/
extern const char* nemo_traverse_one_level_up;   /**< String constant holding the traverse mode "one level up".*/
extern const char* nemo_traverse_all;            /**< String constant holding the traverse mode "all".*/

/**
   @brief
   Find a path to a leaf of the traverse tree for which each Intf is up.

   @param intf Starting point of the traverse tree. Default is "lo".
   @param flag If non-empty, only Intfs for which this flag expression evaluates to true have to be up.
            If there is no such Intf in the path, the path is not considered as a match. Default is empty.
   @param traverse Traverse mode to build the traverse tree. Default is "down".
   @return True if such path is found.
 */
bool nemo_isUp(const char* intf, const char* flag, const char* traverse);

/**
   @brief
   Evaluate a flag expression on the superset of all flags of all Intfs in the traverse tree.

   @param intf Starting point of the traverse tree. Default is "lo".
   @param flag The flag expression to evaluate. Default is empty.
   @param condition Merge only the flags of the Intfs for which this flag expression evaluates to true. Default is empty.
   @param traverse Traverse mode to build the traverse tree. Default is "down".
   @return Result of flag expression evaluation.
 */
bool nemo_hasFlag(const char* intf, const char* flag, const char* condition, const char* traverse);

/**
   @brief
   Set one or more flags on all Intfs in the traverse tree.

   @param intf Starting point of the traverse tree. Default is "lo".
   @param flag The flagset holding all flags to set. Default is empty.
   @param condition Touch only Intfs for which this flag expression evaluates to true. Default is empty.
   @param traverse Traverse mode to build the traverse tree. Default is "this".
 */
void nemo_setFlag(const char* intf, const char* flag, const char* condition, const char* traverse);

/**
   @brief
   Clear one or more flags on all Intfs in the traverse tree.

   @param intf Starting point of the traverse tree. Default is "lo".
   @param flag The flagset holding all flags to clear. Default is empty.
   @param condition Touch only Intfs for which this flag expression evaluates to true. Default is empty.
   @param traverse Traverse mode to build the traverse tree. Default is "this".
   @
 */
void nemo_clearFlag(const char* intf, const char* flag, const char* condition, const char* traverse);

/**
   @brief
   Find an Intf in the traverse tree.

   @param intf Starting point of the traverse tree. Default is "lo".
   @param target The name of the Intf to find. Mandatory.
   @param traverse Traverse mode to build the traverse tree. Default is "down".
   @return True if target was found.
 */
bool nemo_isLinkedTo(const char* intf, const char* target, const char* traverse);

/**
   @brief
   Get the closure of the traverse tree.

   @param intf Starting point of the traverse tree. Default is "lo".
   @param flag Only Intfs for which this flag expression evaluates to true are returned. Default is empty.
   @param traverse Traverse mode to build the traverse tree. Default is "down".
   @return A list of Intf names.

   @warning
   The returned variant_list is allocated on the heap, so it MUST be cleaned up AND freed by
   caller of the function with variant_list_cleanup(), followed by free().
 */
variant_list_t* nemo_getIntfs(const char* intf, const char* flag, const char* traverse);

/**
   @brief
   Get the first Intf of the traverse tree.

   @param intf Starting point of the traverse tree. Default is "lo".
   @param flag Only Intfs for which this flag expression evaluates to true are considered. Default is empty.
   @param traverse Traverse mode to build the traverse tree. Default is "down".
   @return An Intf name.

   @warning
   The returned string is allocated on the heap, so it MUST be freed by the
   caller of the function with free().
 */
char* nemo_luckyIntf(const char* intf, const char* flag, const char* traverse);

/**
   @brief
   Get the values of a specific parameter of all Intfs in the traverse tree.

   @param intf Starting point of the traverse tree. Default is "lo".
   @param name Name or spec of the parameter. Mandatory.
   @param flag Only Intfs for which this flag expression evaluates to true are considered. Default is empty.
   @param traverse Traverse mode to build the traverse tree. Default is "down".
   @return A variant_map that maps Intf names to parameter values. The parameter values are variants, their exact type depends on
        the parameter type.

   @warning
   The returned variant_map is allocated on the heap, so it MUST be cleaned up AND freed by
   caller of the function with variant_map_cleanup(), followed by free().
 */
variant_map_t* nemo_getParameters(const char* intf, const char* name, const char* flag, const char* traverse);

/**
   @brief
   Get the value of a specific parameter of the first Intf in the traverse tree.

   @param intf Starting point of the traverse tree. Default is "lo".
   @param name Name or spec of the parameter. Mandatory.
   @param flag Only Intfs for which this flag expression evaluates to true are considered. Default is empty.
   @param traverse Traverse mode to build the traverse tree. Default is "down".
   @return A parameter value. This is a variant, its exact type depends on the parameter type.

   @warning
   The returned variant is allocated on the heap, so it MUST be cleaned up AND freed by
   caller of the function with variant_cleanup(), followed by free().
 */
variant_t* nemo_getFirstParameter(const char* intf, const char* name, const char* flag, const char* traverse);

/**
   @brief
   Set the values of a specific parameter of all Intfs in the traverse tree.

   @param intf Starting point of the traverse tree. Default is "lo".
   @param name Name or spec of the parameter. Mandatory.
   @param value The value to assign to the parameters. It can be any kind of variant, but preferably of the same type as the parameter.
             Mandatory.
   @param flag Only Intfs for which this flag expression evaluates to true are considered. Default is empty.
   @param traverse Traverse mode to build the traverse tree. Default is "down".
 */
void nemo_setParameters(const char* intf, const char* name, const variant_t* value, const char* flag, const char* traverse);

/**
   @brief
   Set the value of a specific parameter of the first Intf in the traverse tree.

   @param intf Starting point of the traverse tree. Default is "lo".
   @param name Name or spec of the parameter. Mandatory.
   @param value The value to assign to the parameters. It can be any kind of variant, but preferably of the same type as the parameter.
             Mandatory.
   @param flag Only Intfs for which this flag expression evaluates to true are considered. Default is empty.
   @param traverse Traverse mode to build the traverse tree. Default is "down".
 */
void nemo_setFirstParameter(const char* intf, const char* name, const variant_t* value, const char* flag, const char* traverse);

/**
   @brief
   Get entire MIBs of all Intfs in the traverse tree.

   @param intf Starting point of the traverse tree. Default is "lo".
   @param mibs A space seperated list of MIBs to retrieve. Empty string means get all MIBs. Default is empty.
   @param flag Only Intfs for which this flag expression evaluates to true are considered. Default is empty.
   @param traverse Traverse mode to build the traverse tree. Default is "down".
   @return A variant_map that maps MIB names to MIB maps. A MIB map is a variant_map that maps Intf names to object maps.
        An object map is a variant_map that maps parameter names to parameter values (variants) and child object names
        (including instances of templates) to object maps recursively.

   @warning
   The returned variant_map is allocated on the heap, so it MUST be cleaned up AND freed by
   caller of the function with variant_map_cleanup(), followed by free().
 */
variant_map_t* nemo_getMIBs(const char* intf, const char* mibs, const char* flag, const char* traverse);

/**
   @brief
   Set entire MIBs and save persistent data.

   @param intf Dummy Intf. It does not affect the behavior of the function but it must exist. Default is "lo".
   @param mibs A variant_map in the same format as the return value of nemo_getMIBs() [ i.e.
                A variant_map that maps MIB names to MIB maps. A MIB map is a variant_map that maps Intf names to object maps.
                An object map is a variant_map that maps parameter names to parameter values (variants) and child object names
                (including instances of templates) to object maps recursively.
            ]. However, the MIB maps don't have to be complete. Parameters and objects of the MIB that are missing (or discarded
            from the return value of nemo_getMIBs()) are left untouched, except for the instances of template objects:
            for each template object included by a MIB map, any unexisting instance included by the MIB map is created implicitly
            and any existing instance not included by MIB map is deleted implicitly. Mandatory.
 */
void nemo_setMIBs(const char* intf, variant_map_t* mibs);

/**
   @brief
   Create an upper layer Intf -> lower layer Intf dependency.

   @warning Never create circular dependencies, it will result in unstable behavior.

   @param ulintf The name of the upper layer Intf. Mandatory.
   @param llintf The name of the lower layer Intf. Mandatory.
 */
void nemo_linkIntfs(const char* ulintf, const char* llintf);

/**
   @brief
   Delete an upper layer Intf -> lower layer Intf dependency.

   @param ulintf The name of the upper layer Intf. Mandatory.
   @param llintf The name of the lower layer Intf. Mandatory.
 */
void nemo_unlinkIntfs(const char* ulintf, const char* llintf);

/**
   @brief
   Open a query to get the result of a query function and get notified whenever that result changes.

   @details
   This function is a generic method to open a query for any kind of query function. It is recommended to not use this function, but
   to use one of the query function specific wrappers instead. These wrappers do not have the args and attributes arguments to
   generically pass arguments to the query function, but they have a query function specific signature instead, which is more
   convenient.

   Although there is at least a small performance penalty per call to nemo_openQuery(), both the NeMo Client Library and the NeMo
   Core Library have been optimized to maximize the performance of query management and to minimize the overhead of it, so if you
   have a use case for opening the same query 30 times for example, or it is somehow convenient for you to open the same query
   multiple times (different handlers or different userdatas), please go ahead. Identical queries are multiplexed by NeMo and it is
   not needed to invent a likewise mechanism in each application.

   @param intf The Intf to open the query on. This corresponds to the first argument of the actual query function.
   @param subscriber An arbitrary subscriber name.
   @param function The name of the query function. Currently the following functions are supported:
                    - isUp
                    - hasFlag
                    - isLinkedTo
                    - getIntfs
                    - luckyIntf
                    - getFirstParameter
                    - getParameters
                    - getAddrs
                    - luckyAddr
                    - luckyAddrAddress
                    - getDHCPOption
                    .
                Query support in the NeMo Core Library is flexible: it has a public API to register new query functions which
                can be used by external modules, so it is quite possible that this list will be extended in the future.
   @param args The arguments to be passed to the query function.
   @param attributes A bitwise OR of attributes regarding the args argument. Currently the only supported attribute is
                  request_function_args_by_name, defined by <pcb/core/request.h>, indicating that the arguments should be
                  processed by name lookup rather than by order.
   @param handler The callback function to be called with the initial query result and later on whenever the query result changes.
               This function will be called for the first time before the call to nemo_openQuery() finishes. If the query result
               changes later on, the function will be called from within the PCB event loop (more precisely from a PCB reply
               handler).
   @param userdata A void pointer to be passed to the handler.
   @return A pointer to an opaque query object if the openQuery()-call succeeded.

   @warning
   The returned pointer MUST be recorded by the caller and
   passed later on exactly once to nemo_closeQuery(). If the openQuery()-call failed, this function returns a NULL pointer and
   an error will have been logged in the syslog.
 */
nemo_query_t* nemo_openQuery(const char* intf, const char* subscriber, const char* function,
                             argument_value_list_t* args, uint32_t attributes, nemo_result_handler_t handler, void* userdata);

/**
   @brief
   Close an opened query.

   @warning
   This function MUST be called exactly once for each successful call to nemo_openQuery(). By not doing so you're not only
   leaking resources in your application, but also in the NeMo core process. Notice also that nemo_client_cleanup() does NOT close all
   queries automatically.

   @warning
   This function MUST NOT be called from within a query handler. Doing so anyway will crash the application.

   @param query A NULL-pointer (NULL-pointers are ignored) or a pointer to an opaque query object returned by a successful call
             to nemo_openQuery(). When this function returns, this pointer is no longer valid.
 */
void nemo_closeQuery(nemo_query_t* query);

/**
   @brief
   nemo_openQuery() wrapper for querying the function nemo_isUp().

   @param intf see nemo_openQuery() and/or nemo_isUp()
   @param subscriber see nemo_openQuery()
   @param flag see nemo_isUp()
   @param traverse see nemo_isUp()
   @param handler see nemo_openQuery()
   @param userdata see nemo_openQuery()
   @return see nemo_openQuery()
 */
nemo_query_t* nemo_openQuery_isUp(const char* intf, const char* subscriber, const char* flag,
                                  const char* traverse, nemo_result_handler_t handler, void* userdata);

/**
   @brief
   nemo_openQuery() wrapper for querying the function nemo_hasFlag().

   @param intf see nemo_openQuery() and/or nemo_hasFlag()
   @param subscriber see nemo_openQuery()
   @param flag see nemo_hasFlag()
   @param condition see nemo_hasFlag()
   @param traverse see nemo_hasFlag()
   @param handler see nemo_openQuery()
   @param userdata see nemo_openQuery()
   @return see nemo_openQuery()
 */
nemo_query_t* nemo_openQuery_hasFlag(const char* intf, const char* subscriber, const char* flag, const char* condition,
                                     const char* traverse, nemo_result_handler_t handler, void* userdata);

/**
   @brief
   nemo_openQuery() wrapper for querying the function nemo_isLinkedTo().

   @param intf see nemo_openQuery() and/or nemo_isLinkedTo()
   @param subscriber see nemo_openQuery()
   @param target see nemo_isLinkedTo()
   @param traverse see nemo_isLinkedTo()
   @param handler see nemo_openQuery()
   @param userdata see nemo_openQuery()
   @return see nemo_openQuery()
 */
nemo_query_t* nemo_openQuery_isLinkedTo(const char* intf, const char* subscriber, const char* target,
                                        const char* traverse, nemo_result_handler_t handler, void* userdata);

/**
   @brief
   nemo_openQuery() wrapper for querying the function nemo_getIntfs().

   @param intf see nemo_openQuery() and/or nemo_getIntfs()
   @param subscriber see nemo_openQuery()
   @param flag see nemo_getIntfs()
   @param traverse see nemo_getIntfs()
   @param handler see nemo_openQuery()
   @param userdata see nemo_openQuery()
   @return see nemo_openQuery()
 */
nemo_query_t* nemo_openQuery_getIntfs(const char* intf, const char* subscriber, const char* flag,
                                      const char* traverse, nemo_result_handler_t handler, void* userdata);

/**
   @brief
   nemo_openQuery() wrapper for querying the function nemo_luckyIntf().

   @param intf see nemo_openQuery() and/or nemo_luckyIntf()
   @param subscriber see nemo_openQuery()
   @param flag see nemo_luckyIntf()
   @param traverse see nemo_luckyIntf()
   @param handler see nemo_openQuery()
   @param userdata see nemo_openQuery()
   @return see nemo_openQuery()
 */
nemo_query_t* nemo_openQuery_luckyIntf(const char* intf, const char* subscriber, const char* flag,
                                       const char* traverse, nemo_result_handler_t handler, void* userdata);

/**
   @brief
   nemo_openQuery() wrapper for querying the function nemo_getParameters().

   @param intf see nemo_openQuery() and/or nemo_getParameters()
   @param subscriber see nemo_openQuery()
   @param name see nemo_getParameters()
   @param flag see nemo_getParameters()
   @param traverse see nemo_getParameters()
   @param handler see nemo_openQuery()
   @param userdata see nemo_openQuery()
   @return see nemo_openQuery()
 */
nemo_query_t* nemo_openQuery_getParameters(const char* intf, const char* subscriber, const char* name, const char* flag,
                                           const char* traverse, nemo_result_handler_t handler, void* userdata);

/**
   @brief
   nemo_openQuery() wrapper for querying the function nemo_getFirstParameter().

   @param intf see nemo_openQuery() and/or nemo_getFirstParameter()
   @param subscriber see nemo_openQuery()
   @param name see nemo_getFirstParameter()
   @param flag see nemo_getFirstParameter()
   @param traverse see nemo_getFirstParameter()
   @param handler see nemo_openQuery()
   @param userdata see nemo_openQuery()
   @return see nemo_openQuery()
 */
nemo_query_t* nemo_openQuery_getFirstParameter(const char* intf, const char* subscriber, const char* name, const char* flag,
                                               const char* traverse, nemo_result_handler_t handler, void* userdata);

/**
   @deprecated
   "/etc/nemo-defaults.odl" and "/etc/nemo-config.odl" are automatically loaded at startup and there is no use case for loading
   configs at any other time, so this function has been descoped.
 */
static inline void nemo_loadConfig(const char* configfile) {
    (void) configfile;
}

/**
   @deprecated
   An autosave-feature has been added to NeMo: /etc/nemo-config.odl is saved whenever a persistent parameter has changed, obsoleting
   all use cases of this function.
 */
static inline void nemo_saveConfig(const char* configfile) {
    (void) configfile;
}

/**
   @}
 */

#endif
