# libnemo-client

## Summary

Library providing the client interface towards the NeMo plug-in.
It's a wrapper around the NeMo RPC's for ease of use.

## Description

The NeMo plug-in provides the NeMo data model, exporting all known interfaces.
The plug-in has several RPC's to manage these interfaces, to link or unlink them
to each other and to open queries.

This library is a wrapper around this functionality to be used by other plug-in's
or clients that want to interact with NeMo.
It provides a synchronous `C` API and allows installing callback functions upon
an update of a query result.
